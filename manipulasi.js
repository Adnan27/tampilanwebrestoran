const daftarMakanan = [
    {
        judul: "Burger",
        fotoMakanan: "img/2.jpg",
        deskripsiMakanan: "Rp. 50.000",
        stock: 5

    }, {
        judul: "Teh",
        fotoMakanan: "img/10.jpg",
        deskripsiMakanan: "Rp. 4.000",
        stock: 5
    }, {
        judul: "Nasi",
        fotoMakanan: "img/13.jpg",
        deskripsiMakanan: "Rp. 5.000",
        stock: 5
    }, {
        judul: "Ayam bakar",
        fotoMakanan: "img/14.jpg",
        deskripsiMakanan: "Rp. 20.000",
        stock: 5
    }, {
        judul: "Nasi goreng",
        fotoMakanan: "img/15.jpg",
        deskripsiMakanan: "Rp. 15.000",
        stock: 5
    }, {
        judul: "Mie goreng",
        fotoMakanan: "img/16.jpg",
        deskripsiMakanan: "Rp. 12.000",
        stock: 5
    }, {
        judul: "Aqua",
        fotoMakanan: "img/17.jpg",
        deskripsiMakanan: "Rp. 4.000",
        stock: 5
    },  {
        judul: "Susu",
        fotoMakanan: "img/18.jpg",
        deskripsiMakanan: "Rp. 5.000",
        stock: 5
    },  {
        judul: "Stick Kentang",
        fotoMakanan: "img/19.jpg",
        deskripsiMakanan: "Rp. 10.000",
        stock: 5
    },  {
        judul: "Ayam goreng",
        fotoMakanan: "img/20.jpg",
        deskripsiMakanan: "Rp. 10.000",
        stock: 5
    },  {
        judul: "Kentaki",
        fotoMakanan: "img/21.jpg",
        deskripsiMakanan: "Rp. 15.000",
        stock: 5
    },  {
        judul: "Fanta",
        fotoMakanan: "img/12.jpg",
        deskripsiMakanan: "Rp. 5.000",
        stock: 5
    },
];



const callbackMap = (item, index) => {
    const elmnt = document.querySelector('.daftar-makanan');
    
    elmnt.innerHTML += `<div class="card">
    <img src="${item.fotoMakanan}" alt="Avatar" style="width:100%">
    <div class="container">
      <h4><b>${item.judul}</b></h4>
      <p>${item.deskripsiMakanan}</p> 
      
      <button class="button">Pesan</button>
    </div>
  </div>`
}

daftarMakanan.map(callbackMap);





const buttonElmnt = document.querySelector('.button-cari');
buttonElmnt.addEventListener('click', ()=>{
    const hasilPencarian = daftarMakanan.filter((item, index)=>{
                const inputElmnt= document.querySelector('.input-keyword');
                const keyword   = inputElmnt.value.toLowerCase();
                const namaItem  = item.judul.toLowerCase();  
                
                
                return namaItem.includes(keyword);
})
    document.querySelector('.daftar-makanan').innerHTML = '';
    hasilPencarian.map(callbackMap);
});






